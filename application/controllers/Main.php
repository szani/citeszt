<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    //nagybetű kell, hogy legyen. FIGYELM, a fájlnév is legyen nagybetű. Időnként FTP-zés közben átáll kisbetűre, és ledöglik az oldal.
    
    public function __construct() {
        parent::__construct();      
        
        $this->load->model('adatkezeles_model');
    }
    
    
	public function index()
	{
        $this->load
            ->view('head')
            ->view('welcome_message')
            ->view('footer')
            ;
	}
    
    
    public function scss() {
        $this->load
            ->view('head')
            ->view('scss-teszt')
            ->view('footer')
            ;
    }
    
    
    public function myurl($id = '') 
    {
        

        
        $data = array();
        
        $last_id = $this->adatkezeles_model->insert_new_record();
        
        $data['id'] = $last_id;
        
        $this->load
            ->view('head', $data)
            ->view('table')
            ->view('footer')
            ;
    }
}
